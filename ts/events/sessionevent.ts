export interface IEvent_Session {
  userID: string;
  sessionType: 'new' | 'reactivated';
}
