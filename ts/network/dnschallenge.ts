export interface IDnsChallenge {
  hostName: string;
  challenge: string;
}
