export interface IPerson {
  title: string;
  name: string;
  surname: string;
  sex: 'male' | 'female' | 'queer';
}
