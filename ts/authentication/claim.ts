export interface IClaim {
  userName: string;
  userId: string;
  validUntil: number;
  roles: string[];
}
