export interface IVolumeMount {
  hostFsPath: string;
  containerFsPath: string;
}
