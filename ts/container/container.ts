export interface IContainer {
  registryUrl: string;
  tag: string;
  version: string;
}
